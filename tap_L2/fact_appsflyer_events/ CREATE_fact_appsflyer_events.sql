/*
--Table: tap_l2_appsflyer.fact_appsflyer_events
--Pendiente: Performance (Constraints+Index)
--Source: 
   tap_l1_appsflyer_ios.event
   tap_l1_appsflyer_android.event
*/


--DROP TABLE tap_l2_appsflyer.fact_appsflyer_events

--20220127: se agregan columnas af_adset, event_value y campaign

CREATE TABLE tap_l2.fact_appsflyer_events_2
AS
SELECT 
        event_time::date as event_date,
        event_time::timestamp as event_time,
        install_time::timestamp as install_time,
        customer_user_id as external_ref,
    	lower(media_source) as media_source,
        lower(af_channel) as channel,
        lower(event_name) as event_name,
        lower(event_value)
        appsflyer_id,
        lower(af_adset),
        campaign,
        'ios' as device_os,
		_fivetran_synced as _l1_audit_time,
		current_timestamp as _fact_appsflyer_events_audit_time        
FROM tap_l1_appsflyer_ios.event
UNION
SELECT 
        event_time::date as event_date,
        event_time::timestamp as event_time,
        install_time::timestamp as install_time,
        customer_user_id as external_ref,
    	lower(media_source) as media_source,
        lower(af_channel) as channel,
        lower(event_name) as event_name,
        lower(event_value)
        appsflyer_id,
        lower(af_adset),
        campaign,
        'android' as device_os,
		_fivetran_synced as _l1_audit_time,
		current_timestamp as _fact_appsflyer_events_audit_time        
FROM tap_l1_appsflyer_android.event